#!/bin/zsh

cd ~
curl -O https://gitlab.com/mateuszdrewniak/vscode-configuration/-/raw/master/ext.list

echo "Downloaded"
if type say > /dev/null; then
  say Downloaded
fi

cat ~/ext.list | xargs -L 1 code --install-extension

echo "Imported"
if type say > /dev/null; then
  say Imported
fi

rm ~/ext.list
